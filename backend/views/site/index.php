<?php

/**
 *  @var $this yii\web\View
 */

$this->title = 'Admin panel';

use yii\helpers\Url;
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>Complains</h2>

                <p>Sometimes people post offensive things...</p>

                <p><a class="btn btn-default" href="<?= Url::to('/complaints/manage')?>">Manage &raquo;</a></p>
            </div>
            
            <div class="col-lg-4">
                <h2>Users</h2>

                <p>Manage users</p>

                <p><a class="btn btn-default" href="<?= Url::to('/user/manage')?>">Manage &raquo;</a></p>
            </div>
        </div>
        
        <pre>
            <?php var_dump(Yii::$app->user->can('viewComplaintsList')); ?>
        </pre>
        <pre>
            <?php print_r(Yii::$app->user->identity); ?>
        </pre>
    </div>
</div>
