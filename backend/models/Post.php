<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $filename
 * @property string $description
 * @property integer $created_at
 * @property integer $complaints
 */
class Post extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{post}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID Пользователя',
            'filename' => 'Изображение',
            'description' => 'Описание',
            'created_at' => 'Время создания',
            'complaints' => 'Жалобы',
        ];
    }
    
    public static function findComplaints() {
        return Post::find()->where('complaints > 0')->orderBy('complaints DESC');
    }
    
    /**
     * @return string
     */
    public function getImage()
    {  
       return Yii::$app->storage->getFile($this->filename);
    }
    
    /**
     * @return boolean
     */
    public function approve(){
         /*@var redis connection*/
        $redis = Yii::$app->redis;
        $key = "post:{$this->id}:complaints";
                   
        $redis->del($key);
        $this->complaints = 0;
            
        return $this->save(FALSE, ['complaints']);
        
    }
    
    public function deletelikesToRedis()
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;
        $keyLikes = "post:{$this->id}:likes";
        if ($idsUsers = $redis->smembers($keyLikes)) {
            foreach ($idsUsers as $idUser) {
                $redis->srem("user:{$idUser}:likes", $this->id);
            }
            $redis->del($keyLikes);
        }
    }
    
    public function deleteCommentsToRedis()
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;
        $keyComments = "post:{$this->id}:comment";
        
        $redis->del($keyComments);
    }
    
//    public function deletePicture(){
//        if ($this->filename){
//            
//            $dirFile = Yii::$app->storage->getDirFile($this->filename);
//            
//            //Удаляем изображение
//            if(Yii::$app->storage->delDirFile($dirFile)){
//                return true;
//            } 
//        }
//        return false;
//    }
    
    /**
     * Delete images post
     * @return string
     */
    public function deletePicture(){
        if ($this->filename){
            if(Yii::$app->storage->deleteFile($this->filename)){
                return true;
            } 
        }
        return false;
    }
    
    /**
     * @return boolean
     */
    public function delete(){          
        if (!$this->isTransactional(self::OP_DELETE)) {
            /* Удаляем complaints из Redis */ 
            $this->approve();
            /* Удаляем ключи likes из Redis */
            $this->deletelikesToRedis();
            /* Удаляем ключи комментариев из Redis */
            $this->deleteCommentsToRedis();
            /* Удаляем изображение поста */
            $this->deletePicture();
            
            return $result = $this->deleteInternal();
        }

        $transaction = static::getDb()->beginTransaction();
        
        try {
            $result = $this->deleteInternal();
            if ($this->approve() === FALSE && $this->deletelikesToRedis() === FALSE && $this->deleteCommentsToRedis() === FALSE && $this->deletePicture() === FALSE && $result === FALSE) {
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }
            return $result;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
