<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class Comment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{comment}}';
    }
    
    public static function findComment(int $postId)
    { 
        $redis = Yii::$app->redis;
        $key = "post:{$postId}:comment";   
        $ids = $redis->smembers($key);
        
        return Comment::find()->where(['id' => $ids])->all();
    }
}
