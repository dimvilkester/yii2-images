<?php

use yii\db\Migration;

/**
 * Обрабатывает создание таблицы `post`.
 * Handles the creation of table `post`.
 */
class m171023_073416_create_table_post extends Migration
{
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'description' => $this->text(),
            'created_at' => $this->integer()->notNull(),
        ]);

    }

    public function down()
    {
        $this->dropTable('post');
    }

}
