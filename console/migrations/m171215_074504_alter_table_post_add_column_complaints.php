<?php

use yii\db\Migration;

class m171215_074504_alter_table_post_add_column_complaints extends Migration
{
    public function up()
    {
        $this->addColumn('{{%post}}', 'complaints', $this->integer(10));
    }

    public function down()
    {
        $this->dropColumn('{{%post}}', 'complaints');
    }

}
