<?php

use yii\db\Migration;

class m171031_123336_create_table_comment extends Migration
{
    public function up()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'post_id' => $this->integer(11)->notNull(),
            'description' => $this->text(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);
        
        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-comment-user_id-user-id', 
            'comment', 
            'user_id', 
            'user', 
            'id', 
            'CASCADE', 
            'CASCADE'
        );
        
        // add foreign key for table `post`
        $this->addForeignKey(
            'fk-comment-post_id-post-id', 
            'comment', 
            'post_id', 
            'post', 
            'id', 
            'CASCADE', 
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-comment-user_id-user-id', 
            'comment', 
            'user_id', 
            'user', 
            'id'
        );
        
        // drops foreign key for table `post`
        $this->dropForeignKey(
            'fk-comment-post_id-post-id', 
            'comment', 
            'post_id', 
            'post', 
            'id'
        );
        
        $this->dropTable('comment');
    }
}