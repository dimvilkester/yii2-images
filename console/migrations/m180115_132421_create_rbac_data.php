<?php

use yii\db\Migration;
use backend\models\User;

class m180115_132421_create_rbac_data extends Migration
{
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        
        //Define permissions

        //Разрешение 1
        /* Создаем разрешение (объект $viewComplaintsListPermission) "Просмотреть список жалоб" viewComplaintsList */
        $viewComplaintsListPermission = $auth->createPermission('viewComplaintsList');
        /* Сохраняем разрешение в определенную таблицу в базе данных */
        $auth->add($viewComplaintsListPermission);
        
        //Разрешение 2
        $viewPostPermission = $auth->createPermission('viewPost');
        $auth->add($viewPostPermission);
        
        //Разрешение 3
        $deletePostPermission = $auth->createPermission('deletePost');
        $auth->add($deletePostPermission);
        
        //Разрешение 4
        $approvePostPermission = $auth->createPermission('approvePost');
        $auth->add($approvePostPermission);
        
        //Разрешение 5
        $viewUsersListPermission = $auth->createPermission('viewUsersList');
        $auth->add($viewUsersListPermission);
        
        //Разрешение 6
        $viewUserPermission = $auth->createPermission('viewUser');
        $auth->add($viewUserPermission);
        
        //Разрешение 7
        $deleteUserPermission = $auth->createPermission('deleteUser');
        $auth->add($deleteUserPermission);
        
        //Разрешение 8
        $updateUserPermission = $auth->createPermission('updateUser');
        $auth->add($updateUserPermission);
        
        //Define roles
        
        //Роль 1
        $moderatorRole = $auth->createRole('moderator');
        $auth->add($moderatorRole);
        
        //Роль 2
        $administratorRole = $auth->createRole('administrator');
        $auth->add($administratorRole);
        
        //Define roles - permissions relations
        //Устанавливаем соответствие между разрешениями и ролями
        
        //Permission for moderator
        $auth->addChild($moderatorRole, $viewComplaintsListPermission);
        $auth->addChild($moderatorRole, $viewPostPermission);
        $auth->addChild($moderatorRole, $deletePostPermission);
        $auth->addChild($moderatorRole, $approvePostPermission);
        $auth->addChild($moderatorRole, $viewUsersListPermission);
        $auth->addChild($moderatorRole, $viewUserPermission);
        
        //Permission for administrator
        $auth->addChild($administratorRole, $moderatorRole);
        $auth->addChild($administratorRole, $deleteUserPermission);
        $auth->addChild($administratorRole, $updateUserPermission);
        
        /* Create Admin user */
        $user = new User([
            'email' => 'admin@admin.com',
            'username' => 'Admin',
            'password_hash' => '$2y$13$EAm/hW5qoWqT392NgweR..FVeoeJ9AU54SzNp1c4Vw6.OtMzm6WoG', //111111
        ]);
        //Создаем ключ аутентификации пользователя auth_key
        $user->generateAuthKey();
        //Сохраняем пользователя в базу данных
        $user->save();
        
        //Присваиваем роль пользователю
        $auth->assign($administratorRole, $user->getId());
    }
    
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        
        /* Finde Admin user */
        $user = User::find()->where(['username' => 'Admin'])->one();
        /* Remove Admin user of table {user} on 'username' => 'Admin' */
        if($user){
            $user->delete();       
        }
    }

}
