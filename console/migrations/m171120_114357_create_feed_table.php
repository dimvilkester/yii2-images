<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feed`.
 */
class m171120_114357_create_feed_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('feed', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'author_nickname' => $this->string(70),
            'author_name' => $this->string()->notNull(),
            'author_picture' => $this->string(),
            'post_id' => $this->integer()->notNull(),
            'post_filename' => $this->string()->notNull(),
            'post_description' => $this->text(),
            'post_created_at' => $this->integer()->notNull(),
        ]);
                
        // add foreign key for table `post`
        $this->addForeignKey(
            'fk-feed-post_id-post-id', 
            'feed', 
            'post_id', 
            'post', 
            'id', 
            'CASCADE', 
            'CASCADE'
        );

    }  
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `post`
        $this->dropForeignKey(
            'fk-feed-post_id-post-id', 
            'feed',
            'post_id', 
            'post', 
            'id'
        );
        
        $this->dropTable('feed');
    }
}