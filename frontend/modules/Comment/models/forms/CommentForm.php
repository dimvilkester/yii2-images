<?php

namespace frontend\modules\Comment\models\forms;

use yii\base\Model;
use frontend\models\User;
use frontend\models\Post;
use frontend\models\Comment;
use yii\web\NotFoundHttpException;

class CommentForm extends Model
{
    
    const MAX_DESCRIPTION_LENGHT = 1000;

    public $description;
    
    private $id;
    private $user;
    private $post;
    
    /**
     * @param frontend\models\User $user
     */
    public function __construct($id = NULL, User $user, Post $post) {
        $this->id = $id;
        $this->user = $user;
        $this->post = $post;
    }

    /**
     * @return array params
     */
    public function rules() {
        return [
            [['description'], 'string', 'max' => self::MAX_DESCRIPTION_LENGHT],
            [['description'], 'trim'],
            [['description'], 'required'],
         ];
    }
    
    /**
     * @return array params
     */
    public function attributeLabels()
    {
        return [
            'description' => 'Комментарий',
        ];
    }
    
    /**
     * @return boolean
     */
    public function save() {
        if($this->validate()){
            
            $comment = $this->findCommentOnId($this->id);

            $comment->description = $this->description;
            $comment->user_id = $this->user->getId();
            $comment->post_id = $this->post->getId();
            
            if($comment->save(FALSE)){
                $comment->addComment($this->post);
                
                return $this->id = $comment->id;
            }
            
            return FALSE;
        }
    }
    
    /**
     * @param NULL|int $id
     * @return $comment frontend\modules\Comment
     * @throws NotFoundHttpException
     */
    private function findCommentOnId($id){
        if($id){
            if($comment = Comment::findOne($id)){
                return $comment;
            }
            throw new NotFoundHttpException('Comment not found.');
        }
        return $comment = new Comment();
    }
    
    public function getCommentId() {
        return $this->id;
    }
}

