<?php

namespace frontend\modules\Comment\controllers;

use yii\web\Controller;
use Yii;
use frontend\models\Comment;
use frontend\models\Post;
use frontend\modules\Comment\models\forms\CommentForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * Default controller for the `Comment` module
 */
class DefaultController extends Controller
{
    /**
     * @return array params
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'  => ['get', 'post'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'delete' => ['get', 'post'],
                ],
            ],
        ];
    }
    
    /**
     * @param int $postId (post id)
     * @return string
     * @throws NotFoundHttpException
     */   
    public function actionIndex($postId) {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($postId);
        $commentForm = new CommentForm(null, $currentUser, $post);
        
        $limit = Yii::$app->params['maxCommentListOnPage'];
        $page = Yii::$app->request->get('page', 1);
        
        $comments = Comment::findComment($post, $page, $limit)->all();
        
        if (Yii::$app->request->isAjax && Yii::$app->request->get('move') == 1) {
            if(!empty($comments)){
                return $this->renderPartial('_list', [
                    'post' => $post,
                    'currentUser' => $currentUser,
                    'commentForm' => $commentForm,
                    'comments' => $comments,
                ]);
            } else {
                return false;
            }
        }
        
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('index', [
                'post' => $post,
                'currentUser' => $currentUser,
                'commentForm' => $commentForm,
                'comments' => $comments,
            ]);
        }
    }

    /**
     * @param int $postId (post id)
     * @return array
     * @throws NotFoundHttpException
     */   
    public function actionCreate($postId)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($postId);
        $commentForm = new CommentForm(null, $currentUser, $post);
                
        if (Yii::$app->request->isAjax) {           
            if ($commentForm->load(Yii::$app->request->post()) && is_int($commentForm->save())) {
                
                $comment = Comment::findOne($commentForm->getCommentId());

                return $this->renderPartial('_item', [
                    'comment' => $comment,
                    'post' => $post,
                    'currentUser' => $currentUser,
                    'commentForm' => $commentForm
                ]);
            } else {
                return false;
            }
        }
        
        if ($commentForm->load(Yii::$app->request->post()) && $commentForm->save()) {
            Yii::$app->session->setFlash('success', 'Comment update!');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }    
    
    /**
     * @param int $id (comment id)
     * @param int $postId (post id)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $postId)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($postId);
        $commentForm = new CommentForm($id, $currentUser, $post);
        
        if (Yii::$app->request->isAjax) {     
            if ($commentForm->load(Yii::$app->request->post()) && is_int($commentForm->save())) {
                
                Yii::$app->response->format = Response::FORMAT_JSON;
            
                $comment = Comment::findOne($id);
                
                return [
                    'success' => TRUE,
                    'id' => $commentForm->getCommentId(),
                    'description' => $comment->description,
                    'updated_at' => $comment->getUpdatedDatetime(),
                    'text' => 'Comment create',
                ];
            } else {
                return [
                    'success' => FALSE,
                    'text' => 'Error',
                ];
            }
        }
        
        if ($commentForm->load(Yii::$app->request->post()) && $commentForm->save()) {
            Yii::$app->session->setFlash('success', 'Comment update!');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    /**
     * @param int $id (comment id)
     * @param int $postId (post id)
     * @return array
     * @throws NotFoundHttpException
     */   
    public function actionDelete($id, $postId) {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/default/login');
        }

        $post = $this->findPost($postId);
        $comment = Comment::findOne($id);

        if (Yii::$app->request->isAjax) {
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            if ($comment->remComment($post) != NULL && $comment->delete()) {
                return [
                    'success' => TRUE,
                    'count' => $post->countComment(),
                    'text' => 'Delete success!',
                ];
            } else {
                return [
                    'success' => FALSE,
                    'text' => 'Delete failed!',
                ];
            }
        }

        if ($comment->remComment($post) != NULL && $comment->delete()) {
            Yii::$app->session->setFlash('success', 'Delete success!');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            Yii::$app->session->setFlash('danger', 'Delete failed!');
            throw new NotFoundHttpException('Page not found.');
        }
    }

    /**
     * @param int $postId (post id)
     * @return $post frontend\models\Post
     * @throws NotFoundHttpException
     */
    private function findPost(int $postId)
    { 
        if($post = Post::findOne($postId)){
            return $post;
        }
        throw new NotFoundHttpException('Post not found.');
    } 

}
