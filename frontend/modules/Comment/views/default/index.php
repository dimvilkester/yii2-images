<?php
/**
 * @var $this yii\web\View 
 * @var $comments frontend\models\Comment
 * @var $post frontend\models\Post
 * @var $currentUser frontend\models\User
 * @var $commentForm frontend\modules\Comment\models\forms\CommentForm
 */

use yii\helpers\Html;

$this->title = 'Comments';
?>
<div class="row">
    <div class="col-md-9 col-xs-9">
        
            <!-- comments-post begin -->
            <div class="comments-post">
                <div class="single-item-title"></div>
                <div class="row">
                    <ul class="comment-list" data-post-id="<?= Html::encode($post->getId()) ?>">
                        <?php if ($comments): ?>
                            <?= $this->render('_list', [
                                'post' => $post,
                                'currentUser' => $currentUser,
                                'commentForm' => $commentForm,
                                'comments' => $comments,
                            ]) ?>
                        <?php else: ?> 
                            <div class="empty col-md-12">Будьте первым кто оставит комментарий к этой фотографии</div>
                        <?php endif; ?>   
                    </ul>
                </div>
            </div>
            <!-- comments-post end -->         
    </div>
</div>