<?php
/**
 * @var $this yii\web\View 
 * @var $comment frontend\models\Comment
 * @var $post frontend\models\Post
 * @var $currentUser frontend\models\User
 * @var $commentForm frontend\modules\Comment\models\forms\CommentForm
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if ($comment): ?>
<div id="list-view" class="list-view">
    <div class="item" data-key="<?= Html::encode($comment->getId()) ?>">
    <!-- comment item -->
    <li class="comment" id="comment-<?= Html::encode($comment->getId()) ?>">
        <div class="comment-user-image" style="width: 80px;">
            <img src="<?= Html::encode($comment->user->getPicture()) ?>">
        </div>
        <div class="comment-info">
            <h4 class="author">
                <a href="<?= Url::to(['/user/profile/view', 'nickname' => $comment->user->getNickname()]) ?>"><?= Html::encode($comment->user->username) ?></a>
                <span>(<?= Html::encode($comment->updatedDatetime) ?>)</span>

                <?php if ($currentUser && $comment->user->equals($currentUser)): ?>
                    <?= Html::submitButton(
                            '<span class="glyphicon glyphicon-pencil"></span>', ['class' => 'btn btn-default btn-comment-update',
                        'data-id' => Html::encode($comment->getId()),
                        'title' => 'Редактировать',
                    ]) ?>  
                <?php endif; ?>

                <?php if ($post->user && $post->user->equals($currentUser)): ?>
                    <?= Html::submitButton(
                            '<span class="glyphicon glyphicon-trash"></span>', ['class' => 'btn btn-default btn-comment-delete',
                        'data-id' => Html::encode($comment->getId()),
                        'data-post-id' => Html::encode($post->getId()),
                        'title' => 'Удалить',
                    ]) ?>                           
                <?php endif; ?> 
            </h4>
            <div class="display-none" id="comment-update-<?= Html::encode($comment->getId()) ?>">
                <?= $this->render('_formupdate', [
                    'commentForm' => $commentForm,
                    'id' => $comment->getId(),
                    'postId' => $post->getId(),
                    'description' => $comment->description,
                ]) ?>
            </div>

            <div class="description" id="comment-description-<?= Html::encode($comment->getId()) ?>">
                <?= Html::encode($comment->description) ?>
            </div>
        </div>
    </li>
    <!-- comment item -->
    </div>
</div>
<?php endif; ?> 