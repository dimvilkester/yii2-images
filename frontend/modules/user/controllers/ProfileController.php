<?php

namespace frontend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\User;
use yii\web\NotFoundHttpException;
use frontend\modules\user\models\forms\PictureForm;
use yii\web\UploadedFile;
use yii\web\Response;

/**
 * Profile controller for the `user` module
 */
class ProfileController extends Controller {

    /**
     * @param string $nickname
     * @return string
     */
    public function actionView($nickname) {
        
        /*@var $currentUser frontend\models\User*/
        $currentUser = Yii::$app->user->identity;
        
        /*@var $modelPicture frontend\modules\user\models\forms\PictureForm*/
        $modelPicture = new PictureForm();
        
        $user = $this->findUser($nickname);
       
        return $this->render('view', [
            'user' => $user,
            'currentUser' => $currentUser,
            'modelPicture' => $modelPicture,
        ]);
    }
    
    /**
     * @param string $nickname
     * @return $user frontend\models\User
     * @throws NotFoundHttpException
     */
    private function findUser($nickname){
        if ($user = User::find()->where(['nickname' => $nickname])->orWhere(['id' => $nickname])->one()){
            return $user;
        }
        throw new NotFoundHttpException('Page not found');
    }
    
    /**
     * @param integer $id
     * @return frontend\models\User
     * @throws NotFoundHttpException
     */
    private function getUserById($id){
        if ($user = User::findOne($id)){
            return $user;
        }   
        throw new NotFoundHttpException('Page not found');
    }
    
    /**
     * @param integer $id
     * @return redirect
     */
    public function actionSubscribe($id) {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['/user/default/login']);
        }
        /*@var $currentUser frontend\models\User*/
        $currentUser = Yii::$app->user->identity;
        
        $user = $this->getUserById($id);
        
        $currentUser->followUser($user);
        
        return $this->redirect(['/user/profile/view', 'nickname' => $user->getNickname()]);
    }
    
    /**
     * @param integer $id
     * @return redirect
     */
    public function actionUnsubscribe($id) {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['/user/default/login']);
        }
        /*@var $currentUser frontend\models\User*/
        $currentUser = Yii::$app->user->identity;
        
        $user = $this->getUserById($id);
        
        $currentUser->unfollowUser($user);
        
        return $this->redirect(['/user/profile/view', 'nickname' => $user->getNickname()]);
    }
    
    /**
     * Handle profile image upload via json request (Загрузка изображения профиля через запрос json)
     * 
     * т. к. форма ответа указана JSON метод может возвращать массивы,
     * которые автоматически преобразуются в формат JSON
     * @return array
     */
    public function actionUploadPicture() {
        /*@var записываем ответ в формат json*/
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = new PictureForm();
        $model->picture = UploadedFile::getInstance($model, 'picture');
        
        if ($model->validate()){ 
            /*@var $currentUser frontend\models\User*/
            $currentUser = Yii::$app->user->identity;
            
            $currentUser->picture = Yii::$app->storage->saveUploadedFile($model->picture); // 15/27/30379e706840f951d22de02458a4788eb55f.jpg
 
            /*
             * save(FALSE, ..)Валиацию проводить не требуется
             * save(.., ['picture']) сохраняем только атрибут(свойство) picture
             */
            if($currentUser->save(false, ['picture'])){
                return [
                    'success' => true,
                    'pictureUri' => Yii::$app->storage->getFile($currentUser->picture),
                ];
            }
        }
        
        return [
            'success' => false,
            'errors' => $model->getErrors(),
        ];
 
    }
    
    public function actionDeletePicture()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        
        if ($currentUser->deletePicture()) {
            Yii::$app->session->setFlash('success', 'Picture deleted');
        } else {
            Yii::$app->session->setFlash('danger', 'Error occured');
        }
        
        return $this->redirect(['/user/profile/view', 'nickname' => $currentUser->getNickname()]);
    }
    
}