<?php
/* @var $this yii\web\View */
/* @var $user \frontend\models\User */
/* @var $currentUser \frontend\models\User */
/* @var $modelPicture frontend\modules\user\models\forms\PictureForm*/

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use dosamigos\fileupload\FileUpload;
use yii\bootstrap\Modal;

$this->title = Html::encode($user->username);
?>

<div class="page-posts no-padding">
    <div class="row">
        <div class="page page-post col-sm-12 col-xs-12 post-82">
            <div class="blog-posts blog-posts-large">
                <div class="row">
                    <!-- profile -->
                    <article class="profile col-sm-12 col-xs-12">
                        <div class="alert alert-success display-none" id="profile-image-success"><?= Yii::t('profile', 'Profile image updated') ?></div>
                        <div class="alert alert-danger display-none" id="profile-image-fail"></div>
                        
                        <div class="profile-title">
                            <img class="author-image" id="profile-picture" src="<?= $user->getPicture() ?>"/>
                            <div class="author-name"><?= Html::encode($user->username) ?></div>
                            
                            <?php if ($currentUser && $user->equals($currentUser)): ?>
                                <?= FileUpload::widget([
                                    'model' => $modelPicture,
                                    'attribute' => 'picture',
                                    'url' => ['/user/profile/upload-picture'],
                                    'options' => ['accept' => 'image/*'],
                                    'clientEvents' => [
                                        'fileuploaddone' => 'function(e, data) {
                                            if (data.result.success) {
                                                $("#profile-image-success").show();
                                                $("#profile-image-fail").hide();
                                                $("#profile-picture").attr("src", data.result.pictureUri);
                                            } else {
                                                $("#profile-image-fail").html(data.result.errors.picture).show();
                                                $("#profile-image-success").hide();
                                            }
                                         }',
                                    ],
                                ]); ?>

                                <?php if ($currentUser->getPicture() != $currentUser::DEFAULT_IMAGE): ?>    
                                    <a href="<?php echo Url::to(['/user/profile/delete-picture']); ?>" class="btn btn-default"><?= Yii::t('profile', 'Delete picture') ?></a>
                                    <a href="#" class="btn btn-default"><?= Yii::t('profile', 'Edit profile') ?></a>
                                <?php endif; ?>
                            <?php endif; ?>     
                        </div>

                        <?php if($user->about): ?>
                            <hr>
                            <div class="profile-description">
                                <p><?= HtmlPurifier::process($user->about) ?></p>
                            </div>
                        <?php endif; ?>
                        
                        <!-- Если существует текущий пользователь (TRUE) и "другой" пользователь (equals - равняется) -->
                        <?php if ($currentUser && !$user->equals($currentUser)): ?> 
                            <hr>
                            <div class="">
                                <!-- Есть ли у Текущего пользователя Данный пользователь -->
                                <?php if (!$currentUser->isFollowing($user)): ?>
                                    <a href="<?= Url::to(['/user/profile/subscribe', 'id' => $user->getId()]) ?>" class="btn btn-success"><?= Yii::t('profile', 'Subscribe') ?></a>
                                <?php else: ?>
                                    <a href="<?= Url::to(['/user/profile/unsubscribe', 'id' => $user->getId()]) ?>" class="btn btn-danger"><?= Yii::t('profile', 'Unsubscribe') ?></a>
                                <?php endif; ?>
                            </div>        
                            <hr>
                            <!-- Если есть взаимные подписки, тогда показываем блок "Список общих друзей" -->
                            <?php if ($mutualSubscriptions = $currentUser->getMutualSubscriptionsTo($user)): ?>
                                <div class="">
                                    <h5><?= Yii::t('profile', 'Freands, who are also following') ?> <b><?= Html::encode($user->username) ?></b></h5>
                                    <?php foreach ($mutualSubscriptions as $item) : ?>
                                        <p>    
                                            <a href="<?= Url::to(['/user/profile/view', 'nickname' => ($item['nickname']) ? $item['nickname'] : $item['id']]) ?>">
                                               <?= Html::encode($item['username']) ?>
                                            </a>
                                        </p>    
                                    <?php endforeach; ?>    
                                </div>    
                            <?php endif; ?>
                        <?php endif; ?>
                            
                        <hr>    
                        
                        <div class="profile-bottom">
                            <div class="profile-post-count">
                                <span><?= Html::encode($user->getPostCount()) ?> <?= Yii::t('profile', 'Post') ?></span>
                            </div>
                            
                            <div class="profile-followers">
                                <!-- Просмотреть подписки -->  
                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalSubscription">
                                    <?= Html::encode($user->countSubscriptions()) ?> <?= Yii::t('profile', 'Subscriptions') ?>
                                </button>
                            </div>
                            
                            <div class="profile-following">
                                <!-- Просмотреть подписчиков -->  
                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalFollower">
                                    <?= Html::encode($user->countFollowers()) ?> <?= Yii::t('profile', 'Followers') ?>
                                </button> 
                            </div>
                        </div>
                    </article>
                                        
                    <div class="col-sm-12 col-xs-12">
                        <div class="row profile-posts">
                            <?php if($user && $posts = $user->getPost(5)): ?>
                                <?php foreach($posts as $post): ?>
                                    <div class="col-md-4 profile-post">
                                        <a href="<?= Url::to(['/post/default/view', 'postId' => $post->getId()]); ?>">
                                            <img src="<?= Html::encode($post->getImage()) ?>" class="img-thumbnail"/>
                                        </a>
                                        <p><?= Html::encode($post->description) ?></p>
                                        <p><?= Yii::t('profile', 'Likes') ?>: <span class="likes-count badge"><?= Html::encode($post->countLikes()); ?></span></p>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <!-- Modal Subscriptions begin -->
                    <?php Modal::begin([
                            'options' => [
                                'id' => 'modalSubscription',
                            ],
                            'size' => 'modal-md',
                            'header' => '<h4>'. Yii::t('profile', 'Subscriptions') .'</h4>',
                            'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Close</button>',
                        ]); ?>
                        <?php foreach ($user->getSubscriptions() as $subscription): ?>
                            <p>
                                <a href="<?= Url::to(['/user/profile/view', 'nickname' => ($subscription['nickname']) ? $subscription['nickname'] : $subscription['id']]) ?>">
                                    <?= Html::encode($subscription['username']) ?>
                                </a>
                            </p>
                        <?php endforeach; ?>  
                    <?php Modal::end(); ?>
                    <!-- Modal Subscriptions end --> 

                    <!-- Modal Followers begin -->
                    <?php Modal::begin([
                            'options' => [
                                'id' => 'modalFollower',
                            ],
                            'size' => 'modal-md',
                            'header' => '<h4>'. Yii::t('profile', 'Followers') .'</h4>',
                            'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Close</button>',
                        ]); ?>
                        <?php foreach ($user->getFollowers() as $follower): ?>
                            <p>
                                <a href="<?= Url::to(['/user/profile/view', 'nickname' => ($follower['nickname']) ? $follower['nickname'] : $follower['id']]) ?>">
                                    <?= Html::encode($follower['username']) ?>
                                </a>
                            </p>
                        <?php endforeach; ?>  
                    <?php Modal::end(); ?>
                    <!-- Modal Followers end -->                                 
                </div>
            </div>
        </div>
    </div>
</div>
