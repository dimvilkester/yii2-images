<?php

namespace frontend\modules\user\models\forms;

use yii\base\Model;
use Yii;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * PictureForm allows you to upload an image for the user's avatar in the profile
 * @author mr. Anderson
 */
class PictureForm extends Model
{
    public $picture;
    
    public function rules() {
        return[
            [['picture'], 'file', 
                'extensions' => ['jpg'], 
                'maxSize' => $this->getMaxFileSize(),
                'mimeTypes' => 'image/jpeg',
                'checkExtensionByMimeType' => true,
            ],
        ];
    }
    
    // const EVENT_AFTER_VALIDATE наследуется от yii\base\Model
    // Присоединение обработчика - метод объекта
    public function __construct()
    {
        $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'imageResize']);
    }
    
    /**
     * Resize image if needed
     */
    public function imageResize() {
        
        /**
         * Если в объекте UploadedFile произошла ошибка с загруженным файлом
         * Можно проверить $error на содержимое (коды ошибок). 
         * Если true тогда прерываем выполнение скрипта
         */
        if ($this->picture->error) {
            return;
        }
        
        // open an image file
        $img = Image::make($this->picture->tempName); //
        
        // max params
        $maxWidth = Yii::$app->params['profilePicture']['maxWidth'];
        $maxHeight = Yii::$app->params['profilePicture']['maxHeight'];
        
        // prevent possible upsizing
        $img->resize($maxWidth, $maxHeight, function ($constraint) {
            /**
             * Пропорциональное изменение размера с сохранением соотношения сторон изображения.
             */
            $constraint->aspectRatio();
            
            /**
             * Изображения, размером меньше заданных $width, $height не будут изменены:
             */        
            $constraint->upsize();
        });

        // finally we save the image
        $img->save();
    }
    
    /**
     * @return string
     */
    public function getMaxFileSize()
    {
        return Yii::$app->params['maxFileSize'];
    }
}