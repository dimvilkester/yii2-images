<?php

namespace frontend\modules\Post\models\forms;

use Yii;
use yii\base\Model;
use frontend\models\Post;
use frontend\models\User;
use Intervention\Image\ImageManagerStatic as Image;
use frontend\models\events\PostCreatedEvent;

/**
 * Description of PostForms
 *
 * @author mr. Anderson
 * 
 */
class PostForm extends Model{
    
    const MAX_DESCRIPTION_LENGHT = 1000;
    //Определяем событие
    const EVENT_POST_CREATED = "post_created";
    
    public $picture;
    public $description;
    
    private $user;
    
    /**
     * @return array params
     */
    public function rules() {
        return [
            [['picture'], 'file',
                //Проверка на пустоту (Check for empty)
                'skipOnEmpty' => FALSE, 
                'extensions' => 'png, jpg',
                'maxSize' => $this->getMaxFileSize(),
                'mimeTypes' => ['image/jpeg', 'image/png'],
                //Default TRUE
                'checkExtensionByMimeType' => TRUE,
            ],
            [['description'], 'string', 'max' => self::MAX_DESCRIPTION_LENGHT],
        ];
    }
    
    /**
     * @param frontend\models\User $user
     * const EVENT_AFTER_VALIDATE наследуется от yii\base\Model
     * Присоединение обработчика - метод объекта
     */
    public function __construct(User $user) {
        $this->user = $user;
        /**
         * Обработчики события присоединяются с помощью метода yii\base\Component::on().
         */
        $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'imageResize']);
        $this->on(self::EVENT_POST_CREATED, [Yii::$app->feedService, 'addToFeed']);
    }
    
    /**
     * @return boolean
     */
    public function save() {
        
        if($this->validate()){
            
            $post = new Post();
            
            $post->user_id = $this->user->getId();
            $post->filename = Yii::$app->storage->saveUploadedFile($this->picture);
            $post->description = $this->description;
            $post->created_at = time();
            
            if($post->save(FALSE)){
                /**
                 * Инициируем событие при вызове метода yii\base\Component::trigger() 
                 * Инициирование происходит после успешного сохранения записи поста
                 * метода frontend\modules\Post\models\forms\PostForm->save().
                 * 
                 * Методу trigger() передаем имя и объект события, в котором описываются параметры, 
                 * передаваемые обработчикам событий
                 */
                $event = new PostCreatedEvent();
                $event->user = $this->user;
                $event->post = $post;

                $this->trigger(self::EVENT_POST_CREATED, $event);
                return TRUE;
            }
        }
        return FALSE;
    }
    
    /**
     * Resize image if needed
     */
    public function imageResize() {
        
        /**
         * Если в объекте UploadedFile произошла ошибка с загруженным файлом
         * Можно проверить $error на содержимое (коды ошибок). 
         * Если true тогда прерываем выполнение скрипта
         */
        if ($this->picture->error) {
            return;
        }
        
        // open an image file
        $img = Image::make($this->picture->tempName); //
        
        // max params
        $maxWidth = Yii::$app->params['postPicture']['maxWidth'];
        $maxHeight = Yii::$app->params['postPicture']['maxHeight'];
        
        // prevent possible upsizing
        $img->resize($maxWidth, $maxHeight, function ($constraint) {
            /**
             * Пропорциональное изменение размера с сохранением соотношения сторон изображения.
             */
            $constraint->aspectRatio();
            
            /**
             * Изображения, размером меньше заданных $width, $height не будут изменены:
             */        
            $constraint->upsize();
        });

        // finally we save the image
        $img->save();
    }
    
    /**
     * Maximum size of the uploaded file
     * @return integer
     */
    private function getMaxFileSize() {
        return Yii::$app->params['maxFileSize'];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'picture' => '',
            'description' => Yii::t('createpost', 'Image description'),
        ];
    }
}
