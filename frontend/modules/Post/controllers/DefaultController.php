<?php

namespace frontend\modules\Post\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\Post\models\forms\PostForm;
use yii\web\UploadedFile;
use frontend\models\Post;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\Comment\models\forms\CommentForm;

/**
 * Default controller for the `post` module
 */
class DefaultController extends Controller
{
    /**
     * @param int $postId (id post)
     * Renders the create view for the module
     * @return string
     */
    public function actionView(int $postId)
    {      
        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($postId);
        $commentForm = new CommentForm(null, $currentUser, $post);
                
        return $this->render('view', [
            'post' => $post,
            'currentUser' => $currentUser,
            'commentForm' => $commentForm,
        ]);
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        
        $model = new PostForm(Yii::$app->user->identity);
        
        if($model->load(Yii::$app->request->post())){
            
            $model->picture = UploadedFile::getInstance($model, 'picture');          
            
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Post created!');
                return $this->goHome();
            }
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
            
    /**
     * @param int $id (id post)
     * @return $post frontend\models\Post
     * @throws NotFoundHttpException
     */
    private function findPost(int $id)
    { 
        if($post = Post::findOne($id)){
            return $post;
        }
        throw new NotFoundHttpException('Post not found.');
    }   
    
    public function actionLike()
    { 
        if(Yii::$app->user->isGuest){
            return $this->redirect('/user/default/login');
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        /**
         * Получаем id поста из запроса посредствам AJAX (POST заголовка form data)
         * @var int $id 
         */
        $id = Yii::$app->request->post('id');
        
        /**
         * По id поста находим запись из таблицы
         */
        $post = $this->findPost($id);
        
        /**
         * @var $currentUser User frontend\models\User
         */
        $currentUser = Yii::$app->user->identity;
        
        $post->like($currentUser);
        
        return [
            'success' => TRUE,
            'likesCount' => $post->countLikes(),
        ];
    } 
    
    public function actionUnlike()
    { 
        if(Yii::$app->user->isGuest){
            return $this->redirect('/user/default/login');
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        /**
         * Получаем id поста из запроса посредствам AJAX (POST заголовка form data)
         * @var int $id 
         */
        $id = Yii::$app->request->post('id');
        
        /**
         * По id поста находим запись из таблицы
         */
        $post = $this->findPost($id);
        
        /**
         * @var $currentUser User frontend\models\User
         */
        $currentUser = Yii::$app->user->identity;
        
        $post->unlike($currentUser);
        
        return [
            'success' => TRUE,
            'likesCount' => $post->countLikes(),
        ];
    }
    
    public function actionComplain() 
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/user/default/login');
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        /**
         * Получаем id поста из запроса посредствам AJAX (POST заголовка form data)
         * @var int $id 
         */
        $id = Yii::$app->request->post('id');
        
        /**
         * По id поста находим запись из таблицы
         */
        $post = $this->findPost($id);
        
        /**
         * @var $currentUser User frontend\models\User
         */
        $currentUser = Yii::$app->user->identity;
        
        if($post->complain($currentUser)){
            return [
                'success' => TRUE,
                'text' => Yii::t('feed', 'Post reported'),
            ];
        }
        
        return [
            'success' => FALSE,
            'text' => Yii::t('feed', 'Error'),
        ];
    }
}
