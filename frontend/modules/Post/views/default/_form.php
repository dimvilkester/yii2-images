<?php

/**
 * @var $this yii\web\View 
 * @var $model frontend\modules\Post\models\forms\PostForm
 * @var $form yii\bootstrap\ActiveForm
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal col-lg-6',
            ],
        ]);
?>

<?= $form->field($model, 'picture')->fileInput()->hint(Yii::t('createpost', 'Select an image')); ?>

<?= $form->field($model, 'description')->textarea(['rows' => 4, 'cols' => 4])->hint(Yii::t('createpost', 'Enter any text')); ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('createpost', 'Create'), ['class' => 'btn btn-success']) ?>
    <?= Html::resetButton(Yii::t('createpost', 'Reset'), ['class' => 'btn btn-danger']) ?>
</div>
<?php ActiveForm::end(); ?>