<?php
/**
 * @var $this yii\web\View
 * @var $model $frontend\modules\Post\models\forms\PostForm
 */

$this->title = 'Create post';
?>

<div class="site-index">
    <div class="body-content">
        <div class="row">         
            <div class="col-md-12">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
