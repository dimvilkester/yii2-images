<?php
/**
 * @var $this yii\web\View 
 * @var $commentForm frontend\modules\Comment\models\forms\CommentForm
 * @var $form yii\bootstrap\ActiveForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="comment-respond">
    <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'form-comment-create',
                'class' => 'form-comment-create form-group',
            ],
            'action' => ['/comment/default/create', 'postId' => $postId],
        ]);?>

        <p class="comment-form-comment">
            <?= $form->field($commentForm, 'description')
                ->textarea([
                    'rows' => 6,
                    'id' => 'commentform-create-description',
                    'data-id' => $postId,
                    'class' => 'form-control',
                    'placeholder' => Yii::t('createpost', 'Enter text'),
                ])->label('')
            ?>
        </p>
        
        <div class="form-submit">
            <?= Html::submitButton(Yii::t('createpost', 'Send'), ['class' => 'btn btn-secondary btn_com_crt']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>