<?php
/**
 * @var $this yii\web\View
 * @var $post frontend\models\Post
 * @var $currentUser frontend\models\User
 * @var $commentForm frontend\modules\Comment\models\forms\CommentForm
 */
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\commentList\CommentList;
use frontend\assets\PostAsset;

PostAsset::register($this);

$this->title = 'View post';
?>
<div class="page-posts no-padding">                    
    <div class="row">                        
        <div class="page page-post col-sm-12 col-xs-12">
            <div class="blog-posts blog-posts-large">    
                <div class="row">
                    <!-- feed item -->
                    <article class="post col-sm-12 col-xs-12">                                            
                        <div class="post-meta">
                            <div class="post-title">
                                <img src="<?= Html::encode($post->user->getPicture()); ?>" class="author-image" /> 
                                <div class="author-name">
                                    <?php if ($post->user): ?>
                                        <a href="<?= Url::to(['/user/profile/view', 'nickname' => $post->user->getNickname()]) ?>">
                                            <?= Html::encode($post->user->username) ?>
                                        </a>
                                    <?php endif; ?> 
                                </div>
                            </div>
                        </div>
                        <div class="post-type-image">
                            <a href="#">
                                <img src="<?= $post->getImage() ?>" alt="" class="img-thumbnail">
                            </a>
                        </div>
                        <div class="post-description">
                            <p><?= Html::encode($post->description) ?></p>
                        </div>
                        
                        <div class="post-bottom">
                            <div class="post-likes">
                                <a href="#" title="<?= Yii::t('post', 'Like') ?>" class="btn btn-secondary btn-like <?= ($currentUser && $post->isLikedBy($currentUser)) ? Html::encode("display-none") : Html::encode("") ?>" data-id="<?= Html::encode($post->id); ?>">
                                    <i class="fa fa-lg fa-heart"></i>
                                </a>

                                <a href="#" title="<?= Yii::t('post', 'Unlike') ?>" class="btn btn-secondary btn-unlike <?= ($currentUser && $post->isLikedBy($currentUser)) ? Html::encode("") : Html::encode("display-none") ?>" data-id="<?= Html::encode($post->id); ?>">
                                    <i class="fa fa-lg fa-heart-o"></i>
                                </a>
                                
                                <span class="likes-count"><?= Html::encode($post->countLikes()); ?></span> <?= Yii::t('post', 'Likes') ?>
                             </div>
                            
                            <div class="post-comments">
                                <a href="#" title="<?= Yii::t('post', 'Comments') ?>" class="btn-comment-index" data-postId="<?= Html::encode($post->getId()) ?>"><span class="comments-count"><?= Html::encode($post->countComment()) ?></span> <?= Yii::t('feed', 'Comments') ?></a>
                            </div>
                            
                            <div class="post-date">
                                <span><?= Yii::t('post', '{0, date, yyyy MMM. dd HH:mm a}', $post->created_at)?></span> <?= Yii::$app->formatter->asDatetime(Html::encode($post->created_at)) ?>
                            </div>
                            
                            <div class="post-report">
                                <?php if(!$post->isReported($currentUser)): ?>
                                    <a href="#" class="btn btn-secondary btn-complain" data-id="<?= Html::encode($post->getId())?>">
                                        <?= Yii::t('post', 'Report post') ?> <i class="fa fa-cog fa-spin fa-fw icon-preloader" style="display: none;"></i>
                                    </a>
                                <?php else: ?>
                                    <p><?= Yii::t('post', 'Post has been reported') ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </article>
                    <!-- feed item -->

                    <!-- comments begin -->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="post-comments">
                            <h4><span class="comments-count"><?= Html::encode($post->countComment()) ?></span> <?= Yii::t('post', 'Comments') ?></h4>
                        </div>
                        
                        <?= CommentList::widget([
                            'post' => $post,
                            'currentUser' => $currentUser,
                            'commentForm' => $commentForm,
                        ]) ?>
                    </div>
                    <!-- comments end -->
                    
                    <!-- comment form begin -->
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->render('_commentformcreate', [
                            'commentForm' => $commentForm,
                            'postId' => $post->getId(),
                        ]) ?>
                    </div>
                    <!-- comment form end -->
                    
                    <!-- Modal comment create begin -->
                    <div class="modal" id="modal-comment-index" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= Yii::t('feed', 'Comments') ?></h4>
                                </div>
                                
                                <div class="modal-body att"></div>
                                
                                <div class="modal-footer">
                                    <!-- comment form begin -->
                                    <?= $this->render('_commentformcreate', [
                                        'commentForm' => $commentForm,
                                        'postId' => $post->getId(),
                                    ]) ?>
                                    <!-- comment form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal comment create end -->
                </div>
            </div>
        </div>
    </div>
</div>
