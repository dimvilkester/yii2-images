<?php

namespace frontend\widgets\commentList;

use yii\base\Widget;
use frontend\models\Comment;
use yii\data\ActiveDataProvider;

class CommentList extends Widget
{
    public $post;
    public $currentUser;
    public $commentForm;
    
    public function run()
    {      
        $dataProvider = new ActiveDataProvider([
            'query' => Comment::findComment($this->post),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);
        
        return $this->render('list', [
            'commentForm' => $this->commentForm,
            'currentUser' => $this->currentUser,
            'post' => $this->post,
            'dataProvider' => $dataProvider,
        ]); 
    }
}
