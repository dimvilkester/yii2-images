<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if ($model): ?> 
<!--<ul class="comment-list">-->
    <!-- comment item -->
    <li class="comment" id="comment-<?= Html::encode($model->getId()) ?>">
        <div class="comment-user-image" style="width: 80px;">
            <img src="<?= Html::encode($model->user->getPicture()) ?>">
        </div>
        <div class="comment-info">
            <h4 class="author">
                <a href="<?= Url::to(['/user/profile/view', 'nickname' => $model->user->getNickname()]) ?>"><?= Html::encode($model->user->username) ?></a>
                <span>(<?= Html::encode($model->updatedDatetime) ?>)</span>

                <?php if ($currentUser && $model->user->equals($currentUser)): ?>
                    <?= Html::submitButton(
                            '<span class="glyphicon glyphicon-pencil"></span>', ['class' => 'btn btn-default btn-comment-update',
                        'data-id' => Html::encode($model->getId()),
                        'title' => 'Редактировать',
                    ]) ?>
                <?php endif; ?>

                <?php if ($post->user && $post->user->equals($currentUser)): ?>
                    <?= Html::submitButton(
                            '<span class="glyphicon glyphicon-trash"></span>', ['class' => 'btn btn-default btn-comment-delete',
                        'data-id' => Html::encode($model->getId()),
                        'data-post-id' => Html::encode($post->getId()),
                        'title' => 'Удалить',
                    ]) ?> 
                <?php endif; ?> 
            </h4>
            <div class="display-none" id="comment-update-<?= Html::encode($model->getId()) ?>">
                <?= $this->render('_formupdate', [
                    'commentForm' => $commentForm,
                    'id' => $model->getId(),
                    'postId' => $post->getId(),
                    'description' => $model->description,
                ]) ?>
            </div>

            <div class="description" id="comment-description-<?= Html::encode($model->getId()) ?>">
                <?= Html::encode($model->description) ?>
            </div>
        </div>
    </li>
    <!-- comment item -->
<!--</ul>    -->
<?php endif; ?> 