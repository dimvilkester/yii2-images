<?php
/**
 * @var $this yii\web\View 
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $post frontend\models\Post
 * @var $commentForm frontend\modules\Comment\models\forms\CommentForm
 * Class kop\y2sp\ScrollPager
 */

use yii\widgets\ListView;
use kop\y2sp\ScrollPager;
?>
<!-- comments-post begin -->
<div class="comments-post">
    <div class="single-item-title"></div>
    <div class="row">
        <ul class="comment-list">
            <?= ListView::widget([
                'id' => 'list-view',
                'dataProvider' => $dataProvider,
                'showOnEmpty' => false,
                'emptyText' => '<div class="empty col-md-12">Будьте первым кто оставит комментарий к этой фотографии</div>',
                'layout' => "{items}\n{pager}",
                'itemOptions' => ['class' => 'item'],
                'itemView' => '_list',
                'viewParams' => [
                    'currentUser' => $currentUser,
                    'post' => $post,
                    'commentForm' => $commentForm,
                ],
//                'pager' => [
//                    'nextPageCssClass' => 'next',       
//                    'prevPageCssClass' => 'prev',       
//                    'maxButtonCount' => 5,
//                    'options' => [
//                        'class' => 'pagination pagination-lg'
//                    ],
//                ],
                'pager' => [
                    'class' => ScrollPager::className(),
                    'container' => '.list-view',
                    'item' => '.item',
                    'paginationSelector' => '.list-view .pagination',
                    'next' => '.next a',
                    'delay' => 600,
                    'negativeMargin' => 10,
                    'triggerText' => 'Загрузить еще',
                    'triggerTemplate' => '<div class="col-md-12"><div class="ias-trigger" style="text-align: center;"><div class="ias-trigger"><button type="button" class="btn btn-default btn-lg btn-block">{text}</button></div></div>',
                    'triggerOffset' => 0,
                    'spinnerSrc' => '/img/loader_blue_64.gif',
                    'spinnerTemplate' => '<div class="col-md-12"><div class="ias-spinner" style="display: block; text-align: center;"><img src="/img/loader_blue_64.gif" style="display: inline;"/></div></div>',
                    //'noneLeftText' => '<div class="col-md-12"><div style="text-align: center;">Вы достигли конца</div></div>',
                    'noneLeftText' => '',
                    'historyPrev' => '.previous',
                ]
            ]);?>
        </ul>
    </div>
</div>
<!-- comments-post end -->