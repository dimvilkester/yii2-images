<?php
/**
 * @var $this yii\web\View 
 * @var $commentForm frontend\modules\Comment\models\forms\CommentForm
 * @var $form yii\bootstrap\ActiveForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="comment-respond">
    <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'form-comment-update-' .$id,
                'class' => 'form-group',
            ],
            'action' => ['/comment/default/update', 'id' => $id, 'postId' => $postId],
        ]);?>
    
        <?= $form->field($commentForm, 'description')
            ->textarea([
                'id' => 'commentform-update-description',
                'data-id' => $id,
                'data-post-id' => $postId,
                'rows' => 6,
                'class' => 'col-md-6',
                'value' => $description
            ])->label('')
        ?>

        <div class="form-submit">
            <?= Html::submitButton(Yii::t('createpost', 'Send'), ['class' => 'btn btn-secondary btn_com_upt', 'data-id' => $id]) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>