/********************************* Comments begin **************************************/

/* Comment modal begin */

$(document).ready(function () {
    $('a.btn-comment-index').on('click', function () {
        var data = $(this).attr('data-postId');
        $('#modal-comment-index').modal('show');
        
        $('#modal-comment-index').find('.modal-body').load('/comment/default/index?postId=' + data);
        
        return false;
    });
    
    $('.modal-footer').on('submit', '.form-comment-create', function (e) {
        /* preventDefault Отменяет выполнение события. 
         * Например, при "клике" по ссылке, переход по ней не произойдет, 
         * если внутри обработчика будет вызван этот метод.*/
        e.preventDefault();
        
        var id = $('#commentform-create-description').attr('data-id');
        var data = $(this).serialize();
        var emp1 = $('#list-view div.empty');
        var emp2 = $('.modal-body').find('.empty'); 

        $.ajax({
            url: '/comment/default/create?postId=' + id,
            type: 'POST',
            data: data,
            cache: false,
            success: function (item, textStatus) {
                if (textStatus == 'success' && item) {
                    if(emp1 || emp2){
                        emp1.hide();
                        emp2.hide();
                    }

                    $(".comment-list").prepend($(item));
                    
                    $('span.comments-count').text(function(index, text){
                           text = text.substr(0,1);
                           count = parseInt(text);
                           count++;
                           return count; 
                    });
                }
                $('div.modal-footer').find('#form-comment-create')[0].reset();
            },
            error: function (html) {
                console.log('Error ' + html.status);
            }
        });
    });
    
    $('.modal-body').on('click', 'button.btn-comment-update', function(){

        var id = $(this).attr('data-id');
        //console.log(id);
        
        $('.modal-body').on('submit', '#form-comment-update-' + id, function (e) {
            e.preventDefault();
            var postId = $('textarea#commentform-update-description').attr('data-post-id');
            var data = $(this).serialize();
            //console.log(id);
            //console.log(postId);
            $.ajax({
                url: '/comment/default/update?id=' + id + '&' + 'postId=' + postId,
                type: 'POST',
                dataType: 'json',
                data: data,
                cache: false,
                success: function (responsJson) {
                   if(responsJson.success === true){                        
                        $('div#comment-update-' + id).hide();
                        $('div#comment-description-' + id).html(responsJson.description);
                        $('div#comment-description-' + id).show();
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
        });
        
        $('div#comment-update-' + id).toggle();
        $('div#comment-description-' + id).toggle();
    });
});
/* Comment modal end */
 
/**
 * Comment create for post
 */
$(document).ready(function() { 
      $('.home').find('#form-comment-create').on('beforeSubmit', function () {
        var id = $('#commentform-create-description').attr('data-id');
        var data = $(this).serialize();
        var emp = $('#list-view div.empty');

        $.ajax({
            url: '/comment/default/create?postId=' + id,
            type: 'POST',
            data: data,
            cache: false,
            success: function (item, textStatus) {
                if(textStatus == 'success' && item){
                    if(emp){
                        emp.hide();
                    }
 
                    $('span.comments-count').text(function(index, text){
                           text = text.substr(0,1);
                           count = parseInt(text);
                           count++;
                           return count; 
                    });
                    
                    $(".comment-list").prepend($(item));                   
                }
                $('#form-comment-create')[0].reset();
            },
            error: function (html) {
                console.log('Error ' + html.status);
            }
        });
        return false;
    });
});

/**
 * Comment update for post
 */
$(document).ready(function() {
    $('.comments-post').on('click', 'button.btn-comment-update', function(){
        var id = $(this).attr('data-id');
        //console.log(id);
        $('.comments-post').on('submit', '#form-comment-update-' + id, function (e) {           
            e.preventDefault();
            
            var postId = $('textarea#commentform-update-description').attr('data-post-id');
            var data = $(this).serialize();
            //console.log(id);
            //console.log(postId);
            $.ajax({
                url: '/comment/default/update?id=' + id + '&' + 'postId=' + postId,
                type: 'POST',
                dataType: 'json',
                data: data,
                cache: false,
                success: function (responsJson) {
                   if(responsJson.success === true){                        
                        $('div#comment-update-' + id).hide();
                        $('div#comment-description-' + id).html(responsJson.description);
                        $('div#comment-description-' + id).show();
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
        });
        
        $('div#comment-update-' + id).toggle();
        $('div#comment-description-' + id).toggle();
    });
});   

/**
 * Comment delete for post
 */
$(document).ready(function () {
    $('.home').on('click', 'button.btn-comment-delete', function(){ 
        if (confirm('Do you really want to delete?')) {
            
            var id = $(this).attr('data-id');
            var postId = $(this).attr('data-post-id');

            $.ajax({
                url: '/comment/default/delete?id=' + id + '&' + 'postId=' + postId,
                type: 'POST',
                dataType: 'json',
                cache: false,
                success: function (responsJson) {
                    if (responsJson.success === true) {
                        $('div#list-view').find("[data-key='" + id + "']").remove();
                        $("li#comment-" + id).remove();
                        $('span.comments-count').text(responsJson.count);
                        
                        if(responsJson.count === '0'){
                            $('div.modal-body').find('.comment-list').html('<div class="empty col-md-12">Будьте первым кто оставит комментарий к этой фотографии</div>');
                            $('#list-view').html('<div class="empty col-md-12">Будьте первым кто оставит комментарий к этой фотографии</div>');
                        }
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
        } 
        return false;
    });
 });
/********************************* Comments end **************************************/