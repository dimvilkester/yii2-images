/**
 * likes post
 */
$(document).ready(function () {
    $('a.btn-like').click(function () { 
        var btn = $(this);
        var params = {
            'id': $(this).attr('data-id')
        };        
        $.post('/post/default/like', params, function(data) {
            if (data.success) {
                btn.hide();
                btn.siblings('.btn-unlike').show();
                btn.siblings('.likes-count').html(data.likesCount);
            }
            //console.log(data);
        });
        return false;
    });

    $('a.btn-unlike').click(function () { 
        var btn = $(this);
        var params = {
            'id': $(this).attr('data-id')
        };        
        $.post('/post/default/unlike', params, function(data) {
            if (data.success) {
                btn.hide();
                btn.siblings('.btn-like').show();
                btn.siblings('.likes-count').html(data.likesCount);
            }
            //console.log(data);
        });
        return false;
    });

});

