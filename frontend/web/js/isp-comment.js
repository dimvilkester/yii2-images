/********************************* infiniti scroll page comments begin ************************************/
$(document).ready(function () {
    var page;
    var postId;
    
    var partURL = location.search;
    var param = partURL.slice(partURL.indexOf('?') + 1);
    var arrayParam = param.split('&');
    
    var result = [];
    for (var i = 0; i < arrayParam.length; i++) {
        var res = arrayParam[i].split('=');
        result[res[0]] = res[1];
    }

    if (result['page']) {
        page = result['page'];
    } else {
        page = 1;
    }

    var block = true;   
    
    $('div').scroll(function () {
        
        postId = $('div.modal-body').find('.comment-list').data('post-id');
        
        var windowScrollTop = $('div.modal-body').scrollTop();
        var heightDocumentModal = $('div.modal-body').height();
        var heightDocument = $('div.modal-body').find('.comments-post').height();

        if (heightDocumentModal + windowScrollTop >= heightDocument && block) {
            block = false;

            /* Увеличиваем номер страницы на один */
            page++;
            /* ajax запрос на страницу */
            $.ajax({
                url: '/comment/default/index',
                type: 'GET',
                data: 'postId=' + postId + '&page=' + page + '&move=1',
                success: function (commentlist, textStatus) {
                    if (textStatus == 'success' && commentlist) {
                        $(commentlist).appendTo($('div.modal-body').find(".comment-list")).hide().fadeIn(1000);
                        block = true;
                    }
                },
                error: function (html) {
                    console.log('Error ' + html.status);
                },
            });
        }
    });
});
/********************************* infiniti scroll page comments end **************************************/