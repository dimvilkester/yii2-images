/********************************* infiniti scroll page feed begin ************************************/
$(document).ready(function () {
    var page;
    /* The location object contains information about the current URL. 
     Location Object Properties search sets or returns the querystring part of a URL */
    var partURL = location.search;
    /* slice(start, [end]) - Фильтрует набор выбранных элементов, оставляя только те элементы, чьи индексы лежат в определенной области (например от 0 до 5) 
     Метод indexOf() возвращает позицию первого вхождения указанного значения в строку. */
    var param = partURL.slice(partURL.indexOf('?') + 1);
    /* split() - разбивает строку в массив по указанному разделителю. */
    var arrayParam = param.split('&');
    
    var result = [];
    for (var i = 0; i < arrayParam.length; i++) {
        var res = arrayParam[i].split('=');
        result[res[0]] = res[1];
        //console.log(arrayParam[i]);
        //console.log(res);
    }

    if (result['page']) {
        page = result['page'];
    } else {
        page = 1;
    }

    /* Отображаем для наглядности номер страницы в <div class="pager"></div> */
    $(".pager").show().text(page);
    
    var block = true;

    /* Отслеживаем ползунок скролла*/
    $(window).scroll(function () {
        /* Объект window представляет собой открытое окно в браузере. */
        /* Высота окна браузера px*/
        var heightWindowBrowser = $(window).height();
        /* Величина перемещения ползунка скрола px */
        var windowScrollTop = $(window).scrollTop();
        /* Высота документа px */
        var heightDocument = $(document).height();

        if ($(window).height() + $(window).scrollTop() >= $(document).height() && block) {
            /* block = false - блокирует работу блока if. 
             Необходимо для того чтобы при отправке запроса на сервер сперва был получен ответ, 
             прежде чем последует новый запрос на новую порцию данных */
            block = false;
            /* Показывает <div class="load"></div> для отображения спиннера загрузки */
            $(".load").fadeIn(500, function () {
                /* Увеличиваем номер страницы на один */
                page++;
                /* ajax запрос на страницу */
                $.ajax({
                    url: '/site/index',
                    type: 'GET',
                    data: 'page=' + page + '&move=1',
                    success: function (feedlist, textStatus, jqXHR) {
                        if (textStatus == 'success' && feedlist) {
                            $(feedlist).appendTo($("#list-view")).hide().fadeIn(1000);
                            /* меняем значение страницы page блока pager */
                            $(".pager").text(page);
                            /* block = true - разблокирует работу блока if */
                            block = true;
                        }
                        /* скрываем блок load (спиннер загрузки) */
                        $(".load").fadeOut(500);
                    },
                    error: function (html) {
                        console.log('Error ' + html.status);
                    },
                    complete: function(jqXHR, textStatus) {
                        if (textStatus == 'success' && jqXHR.responseText == '') {
                            ($("#list-view")).append('<h3 style="text-align: center;">' + 'Конец страницы' + '</h3>');
                        }
                    }
                });

            });
        }
    });
});
/********************************* infiniti scroll page feed end **************************************/