<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), 
    require(__DIR__ . '/../../common/config/params-local.php'), 
    require(__DIR__ . '/params.php'), 
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    //'language' => 'en-US',
    'language' => 'ru-RU',
    'name' => 'IMAGES',
    'bootstrap' => [
        'log',
        [
            'class' => 'frontend\components\LanguageSelector',
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
        'post' => [
            'class' => 'frontend\modules\Post\Module',
        ],
        'comment' => [
            'class' => 'frontend\modules\Comment\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            //'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index', 
                'about' => 'site/about', 
                'profile/<nickname:\w+>' => 'user/profile/view',
                'post/<postId:\d+>' => 'post/default/view',
                'post/create' => 'post/default/create',
                'comment/index/<postId:\d+>' => 'comment/default/index',
                'comment/create/<postId:\d+>' => 'comment/default/create',
                'comment/update/<id:\d+>/<postId:\d+>' => 'comment/default/update',
                'comment/delete/<id:\d+>/<postId:\d+>' => 'comment/default/delete',
            ],
        ],
        'feedService' => [
            'class' => 'frontend\components\FeedService',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'locale' => 'ru-RU',
            'defaultTimeZone' => 'Europe/Moscow',
            'timeZone' => 'Europe/Moscow',
            'dateFormat' => 'dd MMMM yyyy',
            'datetimeFormat' => 'dd MMMM yyyy HH:mm',
            'timeFormat' => 'HH:mm:ss', 
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@frontend/messages',
                    //'sourceLanguage' => 'en-US',
                ],
            ],
        ],
    ],
    'params' => $params,
    'aliases' => [
        '@frontend' => '/var/www/project/frontend',
    ],    
];
