<?php
return [
    'adminEmail' => 'kiddrtest@gmail.com',
    'supportEmail' => 'kiddrtest@gmail.com',
    'maxFileSize' => 1024 * 1024 * 2, //2 mb
    'storageUri' => '/uploads/', //http:://images.com/uploads/f1/d7/739f9a9c9a99294.jpg
    
    // Настройки могут быть вложенными
    'profilePicture' => [
        'maxWidth' => 1280,
        'maxHeight' => 1024,
    ],
    
    'postPicture' => [
        'maxWidth' => 1024,
        'maxHeight' => 768,
    ],
    
    'feedPostLimit' => 10, // максимальной количество постов в новостной ленты
    'maxCommentListOnPage' => 5, // максимальной количество комментариев в посте
];
