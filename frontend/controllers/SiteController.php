<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\HttpException;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        
        /*@var currentUser frontend\models\User */
        $currentUser = Yii::$app->user->identity;
        $limit = Yii::$app->params['feedPostLimit'];
        $page = Yii::$app->request->get('page', 1);
        $feedList = $currentUser->getFeed($page, $limit);

        if (Yii::$app->request->isAjax && Yii::$app->request->get('move') == 1) {
            if(!empty($feedList)){
                return $this->renderAjax('_feed', [
                    'feedList' => $feedList,
                    'currentUser' => $currentUser,
                ]);
            } else {
                return false;
            }
        }

        return $this->render('index', [
            'feedList' => $feedList,
            'currentUser' => $currentUser,
        ]);
    }
        
    /**
     * Change language
     */
    public function actionLanguage() {
        /* Принимаем язык выбранный пользователем (post) */
        $language = Yii::$app->request->post('language');

        if(isset($language) && in_array($language, ['en-US', 'ru-RU'])){   
            /* Устанавливаем язык для всего приложения */
            Yii::$app->language = $language;
            /* Создаем Cookie */
            $languageCookie = new Cookie([
                'name' => 'language',
                'value' => $language,
                'expire' => time() + 60 * 60 * 24 *30, //30 days
            ]);
            /* Отправляем Cookie пользователю */
            Yii::$app->response->cookies->add($languageCookie);
            
            /* Возвращаем пользователя на ту же страницу где он находился */
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('site', 'Sorry, this language does not exist.'));
            /* Возвращаем пользователя на ту же страницу где он находился */
            return $this->redirect(Yii::$app->request->referrer);
            
            //throw new HttpException(400, Yii::t('site', 'Invalid language value.'));
        }  
    }
    
    /**
     * Displays about us.
     *
     * @return mixed
     */
    public function actionAbout() {               
        return $this->render('about');
    }
}
