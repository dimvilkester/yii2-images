<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * FeedAsset
 */
class FeedAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/isp-feed.js',
        'js/isp-comment.js',
        'js/comment-btn.js',
        'js/complain.js',
        'js/like.js',
    ];
    
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}