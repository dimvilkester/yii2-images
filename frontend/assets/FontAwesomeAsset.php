<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * FontAwesomeAsset. The iconic font and CSS toolkit.
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/font-awesome';

    public $css = [
        'css/font-awesome.css',
    ];
}
