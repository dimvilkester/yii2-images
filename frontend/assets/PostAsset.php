<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * PostAsset
 */
class PostAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/isp-comment.js',
        'js/comment-btn.js',
        'js/complain.js',
        'js/like.js',
    ];
    
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}