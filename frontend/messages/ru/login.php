<?php
return [
    'Please fill out the following fields to login' => 'Пожалуйста, заполните поля для входа',
    'Password' => 'Пароль',
    'Login' => 'Войти',
    'Remember Me' => 'Запомнить меня',
    'If you forgot your password you can' => 'Если вы забыли свой пароль, вы можете его',
    'reset it' => 'сбросить',
    'Login with Facebook' => 'Войти через Facebook',
];



