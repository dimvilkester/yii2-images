<?php
return [
    'Reset password' => 'Сброс пароля',
    'Please choose your new password' => 'Укажите новый пароль',
    'Request password reset' => 'Сброс пароля',
    'Please fill out your email. A link to reset password will be sent there.' => 'Пожалуйста, заполните свой адрес электронной почты. Вам будет отправлена ​​ссылка на сброс пароля.',
    'Send' => 'Отправить',
    'There is no user with this email address.' => 'Пользователь с таким электронным адресом не найден.',
    'Password reset for' => 'Сброс пароля для',
    'Hello ({username})' => 'Здравствуйте ({username})',
    'Follow the link below to reset your password' => 'Для сброса пароля перейдите по ссылке ниже',
    'Save' => 'Сохранить',
    'Password' => 'Пароль',
    'Incorrect email or password.' => 'Неверный адрес электронной почты или пароль.',
    'Password reset token cannot be blank.' => 'Поле пароля не может быть пустым.',
    'Wrong password reset token.' => 'Неверный token сброса пароля.',
    'This username has already been taken.' => 'Имя пользователя уже существует.',
    'This email address has already been taken.' => 'Адрес электронной почты уже существует',
    'Sorry, we are unable to reset password for the provided email address.' => 'Извините, мы не можем сбросить пароль для указанного адреса электронной почты.',
    'Check your email for further instructions.' => 'Проверьте свою электронную почту для получения дальнейших инструкций.',
    'New password saved.' => 'Новый пароль сохранен.',
];



