<?php
return [
    'About IMAGES project' => 'О проекте IMAGES',
    'A free application for sharing photos with social network elements. When uploading a photo, it is pre-processed for correct and convenient display in the application tape.' => 'бесплатное приложение для обмена фотографиями с элементами социальной сети. При загрузке фотографии идет ее предварительная обработка для корректного и удобного отображения в ленте приложения.',
    'The opportunity of virtual friendship, writing a comment for the post and evaluation of photos is realized.' => 'Реализована возможность виртуальной дружбы, написания комментария для поста и оценки фотографий.',
    'A laconic and minimal interface aimed at the rapid development and use of the application for any user.' => 'Лаконичный и минималистичный интерфейс направленный на быстрое освоение и использование приложения для любых пользователей.',
];








