<?php
return [
    'Profile image updated' => 'Изображение профиля обновлено',
    'Delete picture' => 'Удалить изображение',
    'Edit profile' => 'Редактировать профиль',
    'Post' => 'Пост',
    'Subscribe' => 'Подписаться',
    'Unsubscribe' => 'Отписаться',
    'Freands, who are also following' => 'Друзья отслеживающие профиль',
    'Subscriptions' => 'Подписки',
    'Followers' => 'Подписчики',
    'Likes' => 'Нравится',
];



