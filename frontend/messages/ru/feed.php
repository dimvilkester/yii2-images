<?php
return [
    'Unlike' => 'Не нравится',
    'Like' => 'Нравится',
    'Likes' => 'Нравится',
    'Comments' => 'Комментарии',
    'Report post' => 'Пожаловаться на пост',
    'Post reported' => 'Жалоба отправлена',
    'Post has been reported' => 'О жалобе сообщено',
    'Error' => 'Ошибка',
    '{0, date, yyyy MMM. dd HH:mm a}' => '{0, date, dd MMMM yyyy HH:mm}',
];



