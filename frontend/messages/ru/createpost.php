<?php
return [
    'Select an image' => 'Выберите изображение',
    'Enter any text' => 'Введите любой текст',
    'Create' => 'Создать',
    'Reset' => 'Очистить',
    'Image description' => 'Описание изображения',
    'Enter text' => 'Введите текст',
    'Send' => 'Отправить',
];



