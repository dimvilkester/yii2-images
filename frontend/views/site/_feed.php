<?php
/**
 * @var $this yii\web\View 
 * @var $model frontend\models\Feed
 * @var $currentUser frontend\models\User
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<?php if ($feedList): ?>
    <?php foreach ($feedList as $feed): ?>
        <!-- feed item -->
        <article class="post col-sm-12 col-xs-12">                                            
            <div class="post-meta">
                <div class="post-title">
                    <img src="<?= Html::encode($feed->author_picture) ?>" alt="<?= Html::encode($feed->author_name) ?>" class="author-image"/>

                    <div class="author-name">
                        <a href="<?= Url::to(['/user/profile/view', 'nickname' => $feed->author_nickname]) ?>">
                            <?= Html::encode($feed->author_name) ?>
                        </a>
                    </div>
                </div>
            </div>

            <div class="post-type-image">
                <a href="<?= Url::to(['/post/default/view', 'postId' => $feed->post_id]) ?>">
                    <img src="<?= Html::encode($feed->getImage()) ?>" class="img-thumbnail"/>
                </a>
            </div>

            <div class="post-description">
                <p><?= HtmlPurifier::process($feed->post_description) ?></p>
            </div>

            <div class="post-bottom">
                <div class="post-likes">                      
                    <a href="#" title="<?= Yii::t('feed', 'Unlike') ?>" class="btn btn-secondary btn-unlike <?= ($currentUser && $feed->isLikedBy($currentUser)) ? Html::encode("") : Html::encode("display-none") ?>" data-id="<?= Html::encode($feed->post_id); ?>">
                        <i class="fa fa-lg fa-heart-o"></i>
                    </a>

                    <a href="#" title="<?= Yii::t('feed', 'Like') ?>" class="btn btn-secondary btn-like <?= ($currentUser && $feed->isLikedBy($currentUser)) ? Html::encode("display-none") : Html::encode("") ?>" data-id="<?= Html::encode($feed->post_id); ?>">
                        <i class="fa fa-lg fa-heart"></i>
                    </a>

                    <span class="likes-count"><?= Html::encode($feed->countLikes()); ?></span> <?= Yii::t('feed', 'Likes') ?>
                </div>

                <div class="post-comments">
                    <a href="#" title="<?= Yii::t('feed', 'Comments') ?>" class="btn-comment-index" data-postId="<?= Html::encode($feed->post_id) ?>"><?= Html::encode($feed->countComment()) ?> <?= Yii::t('feed', 'Comments') ?></a>
                </div>

                <div class="post-date">
                    <span><?= Yii::t('feed', '{0, date, yyyy MMM. dd HH:mm a}', $feed->post_created_at) ?></span> <?= Html::encode(Yii::$app->formatter->asDatetime($feed->post_created_at)) ?>
                </div>

                <div class="post-report">
                    <?php if (!$feed->isReported($currentUser)): ?>
                        <a href="#" class="btn btn-secondary btn-complain" data-id="<?= Html::encode($feed->post_id) ?>">
                            <?= Yii::t('feed', 'Report post') ?> <i class="fa fa-cog fa-spin fa-fw icon-preloader" style="display: none;"></i>
                        </a>
                    <?php else: ?>
                        <p><?= Yii::t('feed', 'Post has been reported') ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </article>
        <!-- feed item -->
     <?php endforeach; ?>
<?php endif; ?>