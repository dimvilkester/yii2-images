<?php
/**
 * @var $this yii\web\View 
 */
use yii\helpers\Html;

$this->title = 'About';
?>

<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-md-3"></div>
            
            <div class="col-md-6">
                <h1><?= Yii::t('about', 'About IMAGES project')?></h1>
                
                <p><em>IMAGES</em> — <?= Yii::t('about', 'A free application for sharing photos with social network elements. When uploading a photo, it is pre-processed for correct and convenient display in the application tape.')?></p>
                <p><?= Yii::t('about', 'The opportunity of virtual friendship, writing a comment for the post and evaluation of photos is realized.')?></p>
                <p><?= Yii::t('about', 'A laconic and minimal interface aimed at the rapid development and use of the application for any user.')?></p>
            </div>
            
            <div class="col-md-3"></div>
        </div>
    </div>
</div>