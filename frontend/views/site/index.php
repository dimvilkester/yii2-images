<?php
/**
 * @var $this yii\web\View 
 * @var $dataProvider yii\data\ActiveDataProvider 
 * @var $currentUser frontend\models\User
 */

use frontend\assets\FeedAsset;

FeedAsset::register($this);

$this->title = 'Newsfeed';
?>

<div class="page-posts no-padding">                    
    <div class="row">  
        <div class="page page-post col-sm-12 col-xs-12">
            <div class="blog-posts blog-posts-large">    
                <div class="row" id="list-view">
                    <?php if ($feedList): ?>
                        <?= $this->render('_feed', [
                            'feedList' => $feedList,
                            'currentUser' => $currentUser,
                        ]) ?>
                    
                        <div class="load"></div>
                        <div class="pager"></div>
                    <?php else: ?>
                        <p><h2 style="text-align: center;"><?= Yii::t('feed', 'You are not yet signed!') ?></h2></p>
                    <?php endif; ?>
                    
                    <!-- Modal comment create begin -->
                    <div class="modal" id="modal-comment-index" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= Yii::t('feed', 'Comments') ?></h4>
                                </div>
                                
                                <div class="modal-body att"></div>
                                
                                <div class="modal-footer">
                                    <!-- comment form begin -->

                                    <!-- comment form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal comment create end -->
                </div>
            </div>
        </div>
    </div>
</div>