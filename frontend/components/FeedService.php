<?php

namespace frontend\components;

use yii\base\Component;
use yii\base\Event;
use frontend\models\Feed;
/**
 * Description of FeedService
 *
 * @author mr. Anderson
 */
class FeedService extends Component
{
   public function addToFeed(Event $event){
       
       $user = $event->user;
       $post = $event->post;
       
       $followers = $user->getFollowers();
       
       foreach ($followers as $follower) {
           $feedItem = new Feed();
           
           //$feedItem->user_id id подписчика
           $feedItem->user_id = $follower['id'];
           $feedItem->author_id = $user->id;
           $feedItem->author_nickname = $user->getNickname();
           $feedItem->author_name = $user->username;
           $feedItem->author_picture = $user->getPicture();
           $feedItem->post_id = $post->getId();
           $feedItem->post_filename = $post->filename;
           $feedItem->post_description = $post->description;
           $feedItem->post_created_at = $post->created_at;
           
           $feedItem->save();
       }

   }
}
