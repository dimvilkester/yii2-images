<?php

namespace frontend\components;

use yii\base\BootstrapInterface;
/**
 * Description of LanguageSelector
 *
 * @author mr. Anderson
 */
class LanguageSelector implements BootstrapInterface
{
    public $supportedLanguage = ['en-US', 'ru-RU'];

    public function bootstrap($app){
        /* Получаем значение языка из cookies */
        $cookieLanguage = $app->request->cookies['language'];
        
        if(isset($cookieLanguage) && in_array($cookieLanguage, $this->supportedLanguage)){
            /* Устанавливаем язык в приложении из cookies */
            $app->language = $app->request->cookies['language'];
        }
    }
}
