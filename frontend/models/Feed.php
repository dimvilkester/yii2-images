<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "feed".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $author_id
 * @property integer $author_nickname
 * @property string $author_name
 * @property string $author_picture
 * @property integer $post_id
 * @property string $post_filename
 * @property string $post_description
 * @property integer $post_created_at
 */
class Feed extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{feed}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'author_id' => 'Author ID',
            'author_nickname' => 'Author Nickname',
            'author_name' => 'Author Name',
            'author_picture' => 'Author Picture',
            'post_id' => 'Post ID',
            'post_filename' => 'Post Filename',
            'post_description' => 'Post Description',
            'post_created_at' => 'Post Created At',
        ];
    }
    
    /**
     * @return string
     */
    public function getImage()
    {  
       return Yii::$app->storage->getFile($this->post_filename);
    }
    
    /**
     * @return mixed
     */
    public function countLikes(){
         /*@var redis connection*/
        $redis = Yii::$app->redis;
        return $redis->scard("post:{$this->post_id}:likes");
    } 
    
    /**
     * @param \frontend\models\User $user
     */
    public function isLikedBy(User $user){
         /*@var redis connection*/
        $redis = Yii::$app->redis;
        return $redis->sismember("post:{$this->post_id}:likes", $user->getId());
    }
    
    /**
     * @return mixed
     */
    public function countComment(){
        /*@var redis connection*/
        $redis = Yii::$app->redis;
        return $redis->scard("post:{$this->post_id}:comment");
    }
    
    /**
     * @param \frontend\models\User $user
     */
    public function isReported(User $user)
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;
        return $redis->sismember("post:{$this->post_id}:complaints", $user->getId());
    }    
}
