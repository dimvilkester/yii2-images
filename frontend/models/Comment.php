<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $post_id
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class Comment extends ActiveRecord
{    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{comment}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'post_id' => 'Post ID',
            'description' => 'Комментарий',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @info: когда объявляется связь с названием user посредством геттера getUser(), 
     * у вас появляется возможность доступа к свойству user подобно свойству объекта.
     * 
     * @info: Когда PHP не может найти соответствующего публичного свойства 
     * он вызывает соответствующий магический метод (__get() для получения значения 
     * и __set() для установки значения). 
     * 
     * Get author of the post
     * @return User|NULL (Active Query User frontend\modules\user) 
     */
    public function getUser()
    {  
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * @param frontend\modules\Post $post
     */
    public function addComment(Post $post){
        /*@var redis connection*/
        $redis = Yii::$app->redis;
        /* Список комментариев поста */
        return $redis->sadd("post:{$post->getId()}:comment", $this->getId());
    }
    
    /**
     * @param frontend\modules\Post $post
     */
    public function remComment(Post $post){
        /*@var redis connection*/
        $redis = Yii::$app->redis;
        /* Список комментариев поста */
        return $redis->srem("post:{$post->getId()}:comment", $this->getId());
    }
    
    /**
     * @param frontend\modules\Post $post
     * @return mixed
     */
    public function countComment(Post $post){
        /*@var redis connection*/
        $redis = Yii::$app->redis;
        return $redis->scard("post:{$post->getId()}:comment");
    } 

    /**
     * @param frontend\modules\Post $post
     * @return array
     * 
     * Подход жадная загрузка:
     * Посредством вызова метода yii\db\ActiveQuery::with(), указываем объекту Active Record вернуть пользователей с помощью одного SQL-запроса. 
     * В результате снижаете количество выполняемых SQL-запросов от 15 до 11!
     */
    public static function findComment(Post $post, int $page = null, int $limit = null)
    {           
        $order = ['created_at' => SORT_DESC];   
        $start = ($page - 1) * $limit;
        
        $query = Comment::find()->with('user')->where(['post_id' => $post->getId()])->orderBy($order)->limit($limit)->offset($start);
        
        return $result = $query;
    }

    public function getCreatedDatetime()
    {
        return Yii::$app->formatter->asDatetime($this->created_at);
    }
    
    public function setCreatedDatetime($value)
    {
        $this->created_at = strtotime($value);
    }
        
    public function getUpdatedDatetime()
    {
        return Yii::$app->formatter->asDatetime($this->updated_at);
    }
    
    public function setUpdatedDatetime($value)
    {
        $this->updated_at = strtotime($value);
    }

}