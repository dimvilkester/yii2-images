<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $filename
 * @property string $description
 * @property integer $created_at
 */
class Post extends ActiveRecord
{
    /**
     * @return table name
     */
    public static function tableName()
    {
        return '{{post}}';
    }

    /**
     * @return array params
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'filename' => 'Filename',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }
    
    /**
     * @return string
     */
    public function getImage()
    {  
       return Yii::$app->storage->getFile($this->filename);
    }

    /**
     * @info: когда объявляется связь с названием user посредством геттера getUser(), 
     * у вас появляется возможность доступа к свойству user подобно свойству объекта.
     * 
     * @info: Когда PHP не может найти соответствующего публичного свойства 
     * он вызывает соответствующий магический метод (__get() для получения значения 
     * и __set() для установки значения). 
     * 
     * Get author of the post
     * @return User|NULL (Active Query User frontend\modules\user) 
     */
    public function getUser()
    {  
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @param \frontend\models\User $user
     */
    public function like(User $user){
        
        /*@var redis connection*/
        $redis = Yii::$app->redis;
        /* Список пользователей которые лайкнули пост */
        $redis->sadd("post:{$this->getId()}:likes", $user->getId());
        /* Список постов которые лайкнул пользователь */
        $redis->sadd("user:{$user->getId()}:likes", $this->getId());
    }
    
    /**
     * @param \frontend\models\User $user
     */
    public function unlike(User $user){
        
        /*@var redis connection*/
        $redis = Yii::$app->redis;
        /* Список пользователей которые лайкнули пост */
        $redis->srem("post:{$this->getId()}:likes", $user->getId());
        /* Список постов которые лайкнул пользователь */
        $redis->srem("user:{$user->getId()}:likes", $this->getId());
    }
    
    public function getId(){
        return $this->id;
    } 
    
    /**
     * @return mixed
     */
    public function countLikes(){
         /*@var redis connection*/
        $redis = Yii::$app->redis;
        return $redis->scard("post:{$this->getId()}:likes");
    } 
    
    /**
     * @param \frontend\models\User $user
     */
    public function isLikedBy(User $user){
         /*@var redis connection*/
        $redis = Yii::$app->redis;
        return $redis->sismember("post:{$this->getId()}:likes", $user->getId());
    }
    
    /**
     * @return mixed
     */
    public function countComment(){
        /*@var redis connection*/
        $redis = Yii::$app->redis;
        return $redis->scard("post:{$this->getId()}:comment");
    } 
    
    /**
     * Add complaint to post from given user
     * @param \frontend\models\User $user
     * @return boolean
     */
    public function complain(User $user){
         /*@var redis connection*/
        $redis = Yii::$app->redis;
        $key = "post:{$this->getId()}:complaints";
        
        if(!$redis->sismember($key, $user->getId())){
            
            $redis->sadd($key, $user->getId());
            $this->complaints++;
            
            return $this->save(FALSE, ['complaints']);
        }
        
    }
    
    /**
     * @param \frontend\models\User $user
     */
    public function isReported(User $user)
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;
        return $redis->sismember("post:{$this->getId()}:complaints", $user->getId());
    }
}
