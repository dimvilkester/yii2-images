<?php

namespace frontend\models\events;

use yii\base\Event;
use frontend\models\User;
use frontend\models\Post;

/**
 * Description of PostCreatedEvent
 *
 * @author mr. Anderson
 */
class PostCreatedEvent extends Event
{
   /**
    * @var User frontend\models\User
    */
    public $user;
    
   /**
    * @var Post frontend\models\Post
    */
    public $post;
    
    public function getUser() {
        return $this->user;
    }
    
    public function getPost() {
        return $this->post;
    }
}
