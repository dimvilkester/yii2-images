<?php

namespace frontend\tests;

use Yii;
use frontend\tests\fixtures\UserFixture;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    
    /**
     * Метод _before выполняется перед каждым тестом
     * 
     * Для того чтобы тесты могли работать с базой 1 а не с основной 0
     * необходимо переопределить компонент redis и 'database' => 1
     */
    protected function _before()
    {
        Yii::$app->setComponents([
            'redis' => [
                'class' => 'yii\redis\Connection',
                'hostname' => 'localhost',
                'port' => 6379,
                'database' => 1,
            ],
        ]);
    }
    
    /**
     * выполняется после каждого теста (метод)
     */
    protected function _after()
    {
    }
    
    /**
     * Этот метод описывает все фикстуры которые должны быть в этом классе
     */
    public function _fixtures()
    {
        return ['users' => UserFixture::className()];
    } 
    
    /* tests */  
    
    /**
     * Проверяем Nickname на пустоту
     */
    public function testGetNicknameOnNicknameEmpty()
    {
        $user = $this->tester->grabFixture('users', 'user1');
        
        expect($user->getNickname())->equals(1);
     }
     
    /**
     * Проверяем Nickname на присутствие Nickname
     */
    public function testGetNicknameOnNicknameNotEmpty()
    {
        $user = $this->tester->grabFixture('users', 'user2');
        
        expect($user->getNickname())->equals('catelyn');
     }
     
    /**
     * Проверяем колличество постов
     */
    public function testGetPostCount()
    {
        $user = $this->tester->grabFixture('users', 'user1');
        
        expect($user->getPostCount())->equals(3);
    }
    
    /**
     * Проверяем возможность подписок пользователя
     */
    public function testFollowUser()
    {
        $user1 = $this->tester->grabFixture('users', 'user1');
        $user3 = $this->tester->grabFixture('users', 'user3');
        
        //user3 (Jon Snow) подписан на user1 Eddard "Ned" Stark
        $user3->followUser($user1);
        
        $this->tester->seeRedisKeyContains('user:1:followers', 3);
        $this->tester->seeRedisKeyContains('user:3:subscriptions', 1);

        $this->tester->sendCommandToRedis('del', 'user:1:followers');
        $this->tester->sendCommandToRedis('del', 'user:3:subscriptions');
    }
    
    /**
     * Проверяем возможность удаления подписок у пользователя
     */
    public function testUnfollowUser()
    {
        $user1 = $this->tester->grabFixture('users', 'user1');
        $user3 = $this->tester->grabFixture('users', 'user3');
                
        $this->tester->sendCommandToRedis('sadd', 'user:1:followers', 3);
        $this->tester->sendCommandToRedis('sadd', 'user:3:subscriptions', 1);

        //user3 (Jon Snow) отписывается от user1 Eddard "Ned" Stark
        $user3->unfollowUser($user1);
        
        $this->tester->dontSeeInRedis('user:1:followers');
        $this->tester->dontSeeInRedis('user:3:subscriptions');
    }

}