<?php

namespace frontend\tests\models;

use frontend\modules\user\models\SignupForm;
use frontend\tests\fixtures\UserFixture;

class SignupFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    
    protected function _before() {
        parent::_before();
        
        $this->tester->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
            ]
        ]);
    }

    public function testTrimUsername() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => ' some_username ',
            'email' => 'some_email@example.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->username)
                ->equals('some_username');
    }
    
    public function testTrimEmail() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'some_username',
            'email' => ' some_email@example.com ',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->email)
                ->equals('some_email@example.com');
    }
    
    public function testRequiredUsername() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => '',
            'email' => 'some_email@example.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('username'))
                ->equals('Username cannot be blank.');
    }
    
    public function testRequiredEmail() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'some_username',
            'email' => '',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('email'))
                ->equals('Email cannot be blank.');
    }
    
    public function testRequiredPassword() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'some_username',
            'email' => ' some_email@example.com ',
            'password' => '',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('password'))
                ->equals('Password cannot be blank.');
    }
    
    public function testUsernameToShort() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'n',
            'email' => 'some_email@example.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('username'))
                ->equals('Username should contain at least 2 characters.');
    }
    
    public function testPasswordToShort() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'some_username',
            'email' => 'some_email@example.com',
            'password' => '123',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('password'))
                ->equals('Password should contain at least 6 characters.');
    }
    
    public function testUsernameToLong() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'qwertyuiopasdfghjklzxcvbnm',
            'email' => 'some_email@example.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('username'))
                ->equals('Username should contain at most 25 characters.');
    }
    
    public function testEmailToLong() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'some_username',
            'email' => 'qwertyuiopasdf@example.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('email'))
                ->equals('Email should contain at most 25 characters.');
    }

    public function testValidEmailAddress() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'some_username',
            'email' => 'some_email_example.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('email'))
                ->equals('Email is not a valid email address.');
    }
    
    public function testEmailUnique() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'some_username',
            'email' => '3@got.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('email'))
                ->equals('This email address has already been taken.');
    }
    
    public function testUsernameUnique() {
        
        /**
         * Передаем на вход данные форме SignupForm
         */
        $model = new SignupForm([
            'username' => 'Jon Snow',
            'email' => 'some_email@example.com',
            'password' => '123456',
        ]);
        
        $model->signup();
        
        expect($model->getFirstError('username'))
                ->equals('This username has already been taken.');
    }
}