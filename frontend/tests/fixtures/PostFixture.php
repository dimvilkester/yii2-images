<?php

namespace frontend\tests\fixtures;

use yii\test\ActiveFixture;

class PostFixture extends ActiveFixture
{
    /**
     * Указываем с какой моделью связана fixtures
     */
    public $modelClass = 'frontend\models\Post';

}