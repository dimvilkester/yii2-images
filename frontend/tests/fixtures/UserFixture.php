<?php

namespace frontend\tests\fixtures;

use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    /**
     * Указываем с какой моделью связана fixtures
     */
    public $modelClass = 'frontend\models\User';
    
    /**
     * Указываем зависимости таблиц (User зависит от Post)
     */
    public $depends = [
        'frontend\tests\fixtures\PostFixture'
    ];

}