<?php
return [
    'adminEmail' => 'kiddrtest@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'storagePath' => '@frontend/web/uploads/', // /var/www/project/frontend/web/uploads
    'storageUri' => 'http://images.com/uploads/', //http:://admin.images.com/uploads/f1/d7/739f9a9c9a99294.jpg
];
