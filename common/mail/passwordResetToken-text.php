<?php
/* @var $this yii\web\View */
/* @var $user frontend\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/default/reset-password', 'token' => $user->password_reset_token]);
?>

<?= Yii::t('reset', 'Hello ({username}),',[
        'username' => $user->username
    ])?>

<?= Yii::t('reset', 'Follow the link below to reset your password')?>:

<?= $resetLink ?>
