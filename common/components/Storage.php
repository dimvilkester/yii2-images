<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * File storage component
 * @author mr. Anderson
 */
class Storage extends Component implements StorageInterface 
{
    
    private $fileName;

    /**
     * Сохранить заданный экземпляр UploadedFile на диск
     * Save given UploadedFile instance to disk
     * @param $file yii\web\UploadedFile
     * @return string|NULL
     */
    public function saveUploadedFile(UploadedFile $file) {
        
        /*@param string $path путь к файлу, используемый для сохранения загруженного файла*/
        // /var/www/project/frontend/web/uploads/0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg
        $path = $this->preparePath($file);

        /** 
         * saveAs() сохраняет загруженный файл.
         * Если файл $path существует - он будет перезаписан.
         * @return bool true, успешно ли сохранен файл
         * @see error       
         */
        if ($path && $file->saveAs($path)) {
            // 0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg
            return $this->fileName;
        }
    }

    /**
     * Prepare path to save uploaded file
     * @param UploadedFile $file
     * @return string|null
     */
    protected function preparePath(UploadedFile $file) {
        $this->fileName = $this->getFileName($file);
        // 0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg

        $path = $this->getStoragePath() . $this->fileName;
        // /var/www/project/frontend/web/uploads/0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg

        $path = FileHelper::normalizePath($path);
        /**
         * createDirectory() создает директорию
         * dirname — Возвращает имя родительского каталога из указанного пути
         * /var/www/project/frontend/web/uploads/0c/a9
         */
        if (FileHelper::createDirectory(dirname($path))) {
            return $path;
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    protected function getFilename(UploadedFile $file) {
        
        // $file->tempname   -   /tmp/qio93kf
        // sha1 - Secure Hash Algorithm 1 — алгоритм криптографического хеширования.
        $hash = sha1_file($file->tempName); // 0ca9277f91e40054767f69afeb0426711ca0fddd

        /* В строку $hash вставляется '/' слева на 2 символа */
        $name = substr_replace($hash, '/', 2, 0); // 0c/a9277f91e40054767f69afeb0426711ca0fddd
        /* В строку $hash вставляется '/' слева на 5 символов */
        $name = substr_replace($name, '/', 5, 0); // 0c/a9/277f91e40054767f69afeb0426711ca0fddd
        /* string $extension расширение файла. Это свойство доступно только для чтения */
        return $name . '.' . $file->extension;  // 0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg
    }

    /**
     * @return string
     */
    protected function getStoragePath() {
        // /var/www/project/frontend/web/uploads
        return Yii::getAlias(Yii::$app->params['storagePath']);
    }

    /**
     * @param string $fileName
     * @return string
     */
    public function getFile(string $fileName) {
        // /uploads/0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg
        return Yii::$app->params['storageUri'] . $fileName;
    }
    
    /**
     * @param string $filename
     * @return boolean
     */
    public function deleteFile(string $filename)
    {
        $file = $this->getStoragePath().$filename;
        
        if (file_exists($file)) {
            // Если файл существует, удаляем
            return unlink($file);
        }
        
        // Файла нет - хорошо. И удалять не нужно
        return true;
    }
    
    /**
     * Получить дерикторию файла
     * @param string $filename
     * @return string
     */
    public function getDirFile(string $fileName) {
        //Находим первое вхождение подстроки "/" в $fileName и возвращает часть пути $fileName
        $fileDir = mb_stristr($fileName, "/", TRUE); 
        
        // /var/www/project/frontend/web/uploads/0c
        return Yii::getAlias(Yii::$app->params['storagePath']) . $fileDir;
    }
    
    /**
     * Удаление файлов включая вложенные папки (рекурсивное удаление файлов) 
     * @param string $directory
     */
    public function delDirFile(string $directory) {
        // Ищем позицию последнего вхождения подстроки "/" в строку $directory.
        if(strrpos($directory, "/")){
            $directory = rtrim($directory, "/");
        }

        //Находии файловые пути, совпадающие с шаблоном glob() @return array
        if ($objs = glob($directory . "/*")) {
                     
            foreach ($objs as $obj) {
                //Является ли имя файла директорией
                if(is_dir($obj)){
                    $this->delDirFile($obj);
                } else {
                    //Удаляет файл
                    unlink($obj);
                }
            }
        }
        rmdir($directory);
    }
    
}