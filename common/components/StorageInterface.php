<?php

/*
 * StorageInterface необходим для того, чтобы безболезненно для приложения
 * изменить переделать релиз самого компонента сохранения. 
 * Например переписать с нуля
 */

namespace common\components;

use yii\web\UploadedFile;

/**
 * File storage interface 
 * 
 * @author mr. Anderson
 */
interface StorageInterface
{

    public function saveUploadedFile(UploadedFile $file);

    public function getFile(string $filename);
}